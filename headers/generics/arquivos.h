#ifndef ARQUIVOS
#define ARQUIVOS

#include "../headers/includes.h"
#include "lista.h"

//Abre qualquer tipo de arquivo, 0 caso leitura e 1 caso escrita
void abrindoArquivo(FILE** file,char* nomeARQ, int tipo);
//loop para ler e interpretar as informações do arquivo
void lendoArquivo(Arguments *arg,Lista *VETOR_LISTAS);

void nome_arquivo(int tipo,Arguments* arg);

void alter_nome_arquivo(int tipo,Arguments* arg);

void draw_svg(Lista* VETOR_LISTAS,Arguments* arg);

void retangulo_pontilhado(FILE *file,float x,float y,float height,float width,char* id);

void retangulo_figura(FILE *file,float x,float y,float height,float width,char* fillColor,char* perColor);

void retangulo_quadra(FILE *file,float x,float y,float height,float width,char* id,char* fillColor,char* perColor);

void circulo_pontilhado(FILE *file,float x,float y,float radius);

void circulo_figura(FILE *file,float x,float y,float radius,char* fillColor,char* perColor);

void closingSVG(FILE *arqsvg);

void openingSVG(FILE* file);

int parando_arquivo(Arguments* arg,char* inst);
#endif