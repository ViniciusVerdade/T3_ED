#ifndef CLOSEST_PAIR
#define CLOSEST_PAIR

#include "../headers/includes.h"
#include <float.h>

struct point
{
    int x, y;
    char* id;
};
typedef struct point Point;

void closest_pair(); 

int compareX(const void* a, const void* b);

int compareY(const void* a, const void* b);

void margex(Point **vetor, int comeco, int meio, int fim);

void margey(Point **vetor, int comeco, int meio, int fim);

void margeSorty(Point **vetor, int comeco, int fim);

void margeSortx(Point **vetor, int comeco, int fim);

void *CriarMenorDist();

float dist(Point *p1, Point *p2);

void *bruteForce(Point **P, int n, void *menor);

void *stripClosest(Point **strip, int size, float min);

void *closestUtil(Point **Px, Point **Py, int n);

void closest_pair(Lista LISTA_TORRES);

#endif 