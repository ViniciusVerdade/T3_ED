#ifndef ESTRUTURA
#define ESTRUTURA

#include "includes.h"

enum{
    QUADRA,
    HIDRANTE,
    SEMAFORO,
    TORRE_RADIO,
    FIGS,
    FIGS_EXTRA,
    EQ_URBANO,
    RETANGULO,
    CIRCULO

};

struct figur{
    char* id;
    float radius;
    float width;
    float height;
    float x;
    float y;
    float xcenter;
    float ycenter;
    char *perColor; /*cor da borda*/
    char *fillColor; /*cor do preenchimento*/
};
typedef struct figur Figure;

struct arg{
    char* path; // -e
    char* directory;// -o
    char* filePosGEO;// soma do path + directory + fileName
    char* filePosQRY;// soma do path + directory + fileName
    char* geoFile; // -f
    char* qryFile;
    char* txtFile;
    char* svgFile;
    FILE *mainFile; //tirar para abrir direto na main
    FILE *sFile;
    FILE *tFile;
    int parada;
};
typedef struct arg Arguments;

struct col{
    char* corFill;
    char* corBorder;
};
typedef struct col Colors;

Colors *newColors();

Arguments *newArguments();
Figure *newFigure();
void freeFigures(Figure **fig);

void freeArguments(Arguments **arg);

#endif