#ifndef INCLUDES
#define INCLUDES


#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "struct.h"
#include "parametros.h"
#include "comandos.h"
#include "figuras.h"
#include "./generics/uteis.h"
#include "./generics/arquivos.h"
#include "./generics/lista.h"
#include "./generics/closest_pair.h"

#endif