#include "../headers/includes.h"

Arguments *argumentos(int argc,const char* argv[] )
{
    int i,j,k;/* contadores*/
    int val1=0,val2=0;/*variavel*/
    Arguments *argumentos = newArguments();
    /*todo IMPLEMENTAR A CORREÇÃO PARA CASO SEJA INSERIDO OU NÃO "/" NO FINAL DO PATH E DO DIRETÓRIO*/
    for(i=1 ; i < argc ; i++)
    {
        if( strcmp(argv[i] ,"-f") == 0) /*? TRABALHANDO O NOME DO ARQUIVO*/
        {
            j = i+1;

            argumentos->geoFile =(char*)calloc(strlen(argv[j])+1,sizeof(char));
            
            strcpy(argumentos->geoFile,argv[j]);
        }

        if( strcmp(argv[i] ,"-o") == 0)/*? TRABALHANDO O DIRETORIO EM QUE SERÁ CRIADO OS ARQUIVOS*/
        {
            j = i+1;

            argumentos->directory = (char*)calloc(strlen(argv[j])+1,sizeof(char));
            strcpy(argumentos->directory, argv[j]);
        }
        
        if( strcmp(argv[i],"-e") == 0) 
        {
            j = i+1;
            val1 = 1;

            argumentos->path = (char*)calloc(strlen(argv[j])+1,sizeof(char));
            strcpy(argumentos->path, argv[j]);
            k = strlen(argumentos->path)-1;
        }

        if( strcmp(argv[i],"-q") == 0)
        {
            j = i+1;
            val2 = 2;

            argumentos->qryFile =(char*)calloc(strlen(argv[j])+1,sizeof(char));
            strcpy(argumentos->qryFile, argv[j]);
        }
    }
//
    if(argumentos->geoFile[0] == '/')
    {
        shiftLeft(argumentos->geoFile,1) ;
    }
    if(argumentos->qryFile)    
    {
        if(argumentos->qryFile[0] == '/')
        {
            shiftLeft(argumentos->qryFile,1) ;
        }
    }
    if(argumentos->path) 
    {
        if(argumentos->path[k] != '/')
        {
            strcat(argumentos->path,"/");
        }
    }
    i = strlen(argumentos->directory)-1;
    if(argumentos->directory[i] != '/')
    {
        strcat(argumentos->directory,"/");
    }
//
    if(val1 == 1) 
    {
        argumentos->filePosGEO = (char*)calloc(strlen(argumentos->path)+strlen(argumentos->geoFile)+1,sizeof(char));
        strcpy(argumentos->filePosGEO,argumentos->path);
        strcat(argumentos->filePosGEO,argumentos->geoFile);
    }
    else
    {
        argumentos->filePosGEO = (char*)calloc(strlen(argumentos->geoFile)+1,sizeof(char));
        strcat(argumentos->filePosGEO,argumentos->geoFile);
    }
    
    if(val2 == 2) 
    {
        if(val1 == 1)
        {
            argumentos->filePosQRY = (char*)calloc(strlen(argumentos->path)+strlen(argumentos->qryFile)+1,sizeof(char));
            strcpy(argumentos->filePosQRY,argumentos->path);
            strcat(argumentos->filePosQRY,argumentos->qryFile);
        }
        else
        {
            argumentos->filePosQRY = (char*)calloc(strlen(argumentos->qryFile)+1,sizeof(char));
            strcat(argumentos->filePosQRY,argumentos->qryFile);
        }
    }
    nome_arquivo(TXT,argumentos);
    nome_arquivo(SVG,argumentos);

    return argumentos;
}

