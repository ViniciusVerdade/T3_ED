#include "includes.h"
#include <math.h>

int caractLinha( FILE *fin )
{
    int c, count;

    count = 0;
    for( ;; )
    {
        c = fgetc( fin );
        if( c == EOF || c == '\n' )
            break;
        ++count;
    }
	fseek(fin,-(count+1),SEEK_CUR);
    return count+1;
}
void pularLinha(FILE* var)
{
    if(!feof(var))
    {
        fseek(var,1,SEEK_CUR);
	}
}

void getLine(char* aux,FILE* file,int caract)
{
    int i;
    fseek(file,-1,SEEK_CUR);
    for(i=0;i<caract+1;i++)
    {
        aux[i] = fgetc(file);
        if(aux[i] == '\n')
        {
            aux[i] = '\0';
            break;
        }
    }
    // fgets(aux,caract+1,file);
}

float max(float a, float b) 
{
    return (a > b) ? a : b;
}

float min(float a, float b) 
{
    return (a < b) ? a : b;
}

void shiftLeft(char* text, int n) 
{ 
    int i;
    int tamanho = strlen(text);
    for ( i = 0; i < tamanho - n + 1; i++)
    {
        text[i] = text[i + n];

    }

}

float dotDistance(float x1,float y1,float x2,float y2)
{
    float d;
    
    if(x1 >= x2)
    {
        if(y1 >= y2)
        {
            d = pow( ( y1 - y2 )*( y1 - y2 ) + ( x1 - x2 )*( x1 - x2 ),0.5 );
        }
        else
        {
            d =  pow( ( y2 - y1 )*( y2 - y1 ) + ( x1 - x2 )* ( x1 - x2 ),0.5);
        }
    }
    else
    {
        if(y1 >= y2)
        {
            d =  pow( ( y1 - y2 )*( y1 - y2 ) + ( x2 - x1 )*( x2 - x1 ),0.5);
        }
        else
        {
            d =  pow( ( y2 - y1 )*( y2 - y1 ) + ( x2 - x1 )*( x2 - x1 ),0.5);
        }
    }

    return d;
}

char* alocarMemoria(int caract)
{
	return (char*)calloc( (caract+1), sizeof(char));
}

char* cut_until_char(char* palavra)
{
    int i, val, tam;
    tam = strlen(palavra);
    val = tam;
    for(i=0; i<=tam;i++ )
    {
        if(palavra[i] == ' '||palavra[i] == '\n'||palavra[i] == '\0')
        {
            val = i+1;
            break;
        }
    }
    shiftLeft(palavra,val);
    return palavra;
}

// char* get_palavra_until_char(char* palavra, char esc,char* aux2)
// {
//     int i, val;

//     for(i=0; i<strlen(palavra);i++ )
//     {
//         if(palavra[i] == esc||palavra[i] == '\n')
//         {
//             val = i+1;
//             break;
//         }
//     }
//     strncpy(aux2,palavra,val-1);
//     // for(i=0; i<val;i++ )
//     // {
//     //     aux2[i] = palavra[i];
//     // }
//     // aux2[val-1] = '\0';
//     return aux2;
// }

char* separate_char(char* aux,char*aux2)
{
    int i, val, tam;

    tam = strlen(aux);
    val = tam;

    for(i=0; i<=tam;i++ )
    {
        if(aux[i] == ' '||aux[i] == '\n' || aux[i] == '\0')
        {
            val = i+1;
            break;
        }
    }
    strcpy(aux2,aux);
    aux2[val-1] = '\0';
    // strncpy(aux2,aux,val-1);
    aux = cut_until_char(aux);
    return aux2;
}