#include "../headers/includes.h"
// A structure to represent a Point in 2D plane
/* Following two functions are needed for library function qsort().
   Refer: http://www.cplusplus.com/reference/clibrary/cstdlib/qsort/ */
 
/*Mergesorts*/
#define FLOAT_MAX 9999999999999
#include <math.h>
typedef struct menordistancia{
    char *id1;
    float x1;
    float y1;
    char *id2;
    float x2;
    float y2;
    float dist;
}Menor_distancia;

Point** transforming_list(Lista LISTRA_REQUERIDA)
{
    Posic p;
    int i=0;
    Point **vetor_pontos = (Point**)calloc(length_lista(LISTRA_REQUERIDA),sizeof(Point*));
    void* objeto;

    p = get_first_lista(LISTRA_REQUERIDA);
    while(p)
    {
        objeto = get_lista(LISTRA_REQUERIDA,p);
        vetor_pontos[i] = (Point*)malloc(sizeof(Point));

        vetor_pontos[i]->x = ((struct Radio*)objeto)->x;
        vetor_pontos[i]->y = ((struct Radio*)objeto)->y;
        vetor_pontos[i]->id = alocarMemoria( strlen( ((struct Radio*)objeto)->id) );
        strcpy(vetor_pontos[i]->id,((struct Radio*)objeto)->id);
        i++;
        p = get_next_lista(LISTRA_REQUERIDA,p);
    }
    return vetor_pontos;
}


void margex(Point **vetor, int comeco, int meio, int fim)
{
    int com1 = comeco, com2 = meio+1, comAux = 0, tam = fim-comeco+1;
    Point **vetAux;
    vetAux = malloc(tam * sizeof(Point*));

    while(com1 <= meio && com2 <= fim){
        if(vetor[com1]->x < vetor[com2]->x) {
            vetAux[comAux] = vetor[com1];
            com1++;
        } else {
            vetAux[comAux] = vetor[com2];
            com2++;
        }
        comAux++;
    }

    while(com1 <= meio){  /*Caso ainda haja elementos na primeira metade*/
        vetAux[comAux] = vetor[com1];
        comAux++;
        com1++;
    }

    while(com2 <= fim) {   /*Caso ainda haja elementos na segunda metade*/
        vetAux[comAux] = vetor[com2];
        comAux++;
        com2++;
    }

    for(comAux = comeco; comAux <= fim; comAux++){    /*Move os elementos de volta para o vetor original*/
        vetor[comAux] = vetAux[comAux-comeco];
    }
    
    free(vetAux);
}

void margey(Point **vetor, int comeco, int meio, int fim) 
{
    int com1 = comeco, com2 = meio+1, comAux = 0, tam = fim-comeco+1;
    Point **vetAux;
    vetAux = malloc(tam * sizeof(Point*));

    while(com1 <= meio && com2 <= fim){
        if(vetor[com1]->y < vetor[com2]->y) {
            vetAux[comAux] = vetor[com1];
            com1++;
        } else {
            vetAux[comAux] = vetor[com2];
            com2++;
        }
        comAux++;
    }

    while(com1 <= meio){  /*Caso ainda haja elementos na primeira metade*/
        vetAux[comAux] = vetor[com1];
        comAux++;
        com1++;
    }

    while(com2 <= fim) {   /*Caso ainda haja elementos na segunda metade*/
        vetAux[comAux] = vetor[com2];
        comAux++;
        com2++;
    }

    for(comAux = comeco; comAux <= fim; comAux++){    /*Move os elementos de volta para o vetor original*/
        vetor[comAux] = vetAux[comAux-comeco];
    }
    
    free(vetAux);
}

void margeSorty(Point **vetor, int comeco, int fim)
{
    if (comeco < fim) {
        int meio = (fim+comeco)/2;
        margeSorty(vetor, comeco, meio);
        margeSorty(vetor, meio+1, fim);
        margey(vetor, comeco, meio, fim);
    }
}

void margeSortx(Point **vetor, int comeco, int fim)
{
    if (comeco < fim) {
        int meio = (fim+comeco)/2;
        margeSortx(vetor, comeco, meio);
        margeSortx(vetor, meio+1, fim);
        margex(vetor, comeco, meio, fim);
    }
}

void *CriarMenorDist()
{
    Menor_distancia *menor = malloc(sizeof(Menor_distancia));
    
    menor->id1 = malloc(50 * sizeof(char));

    menor->id2 = malloc(50 * sizeof(char));

 return (void*)menor;
}

/* A divide and conquer program in C++ to find the smallest distance from a
given set of points.*/

/* A utility function to find the distance between two points*/
/*usando a struct point*/
float dist(Point *p1, Point *p2)
{
    return sqrt( (p1->x - p2->x)*(p1->x - p2->x) + (p1->y - p2->y)*(p1->y - p2->y));
}

/* A Brute Force method to return the smallest distance between two points
in P[] of size n*/
void *bruteForce(Point **P, int n, void *menor)
{
    float min = FLOAT_MAX;
    int i, j;
    j=0;
    for (i = 0; i < n; ++i)
        for (j = i+1; j < n; ++j)
            if (dist(P[i], P[j]) < min){
                min = dist(P[i], P[j]);
                // setDist(FLOAT_MAX, menor);
                // setX1(P[i]->x, menor);
                // setY1(P[i]->y, menor);
                // setX2(P[j]->x, menor);
                // setY2(P[j]->y, menor);
                // setID1(P[i]->id, menor);
                // setID2(P[j]->id, menor);
                ((Menor_distancia*)menor)->dist = FLOAT_MAX;
                ((Menor_distancia*)menor)->x1 = P[i]->x;
                ((Menor_distancia*)menor)->y1 = P[i]->y;
                ((Menor_distancia*)menor)->x2 = P[j]->x;
                ((Menor_distancia*)menor)->x2 = P[j]->x;
                strcpy(((Menor_distancia*)menor)->id1,P[i]->id);
                strcpy(((Menor_distancia*)menor)->id2,P[j]->id);
            }
    return menor;
}

/* A utility function to find minimum of two float values*/

/* A utility function to find the distance beween the closest points of
strip of given size. All points in strip[] are sorted accordint to
y coordinate. They all have an upper bound on minimum distance as d.
Note that this method seems to be a O(n^2) method, but it's a O(n)
method as the inner loop runs at most 6 times*/
void *stripClosest(Point **strip, int size, float min)
{
    void *menor = CriarMenorDist();
    int i;
    int j;
    for (i = 0; i < size; ++i)
        for (j = i+1; j < size && (strip[j]->y - strip[i]->y) < min; ++j)
            if (dist(strip[i], strip[j]) < min){
                min = dist(strip[i], strip[j]);
                ((Menor_distancia*)menor)->dist = FLOAT_MAX;
                ((Menor_distancia*)menor)->x1 = strip[i]->x;
                ((Menor_distancia*)menor)->y1 = strip[i]->y;
                ((Menor_distancia*)menor)->x2 = strip[j]->x;
                ((Menor_distancia*)menor)->x2 = strip[j]->x;
                strcpy(((Menor_distancia*)menor)->id1,strip[i]->id);
                strcpy(((Menor_distancia*)menor)->id2,strip[j]->id);
            }
    return menor;
}
void *closestUtil(Point **Px, Point **Py, int n)
{
    /* Find the middle point*/
    int mid = n/2;
    /* Divide points in y sorted array around the vertical line.
    Assumption: All x coordinates are distinct.*/
    Point **Pyl = malloc(sizeof(Point*) * (mid+1));   /* y sorted points on left of vertical line*/
    Point **Pyr = malloc(sizeof(Point*) * (n-mid-1));  /* y sorted points on right of vertical line*/
    Point *midPoint = Px[mid];
    int li = 0, ri = 0;  /* indexes of left and right subarrays*/
    int i;
    int j=0,qtdL = 0,qtdR=0;
    void *dl;
    void *dr;
    void *d;
    void* p;
    void* stripClsDist;
    int val;
    /* Build an array strip[] that contains points close (closer than d)
    to the line passing through the middle point*/
    Point **strip = malloc(n * sizeof(Point*));
    /* If there are 2 or 3 points, then use brute force*/
    void *menor = CriarMenorDist();
    if (n <= 3)
        return bruteForce(Px, n, menor);

    for (i = 0; i < n; i++)
    { //definido os pontos a direita e a esquerda
        if(Py[i] == NULL)
        {
            break;
        }
        if (Py[i]->x <= midPoint->x)
            Pyl[li++] = Py[i];
        else
            Pyr[ri++] = Py[i];

    }

    /* Consider the vertical line passing through the middle point
    calculate the smallest distance dl on left of middle point and
    dr on right side*/
    
    dl = closestUtil(Px, Pyl, mid); 
    
    dr = closestUtil(Px, Pyr, mid);

    /* Find the smaller of two distances*/
    if( ((Menor_distancia*)dl)->dist <= ((Menor_distancia*)dr)->dist)
    {
        d = dl;
    }
    else
    {
        d = dr;
    }


    for (i = 0; i < n; i++)
        if (abs(Py[i]->x - midPoint->x) < ((Menor_distancia*)d)->dist){
            strip[j] = Py[i];
            j++;
        }

    stripClsDist = stripClosest(strip, j, ((Menor_distancia*)d)->dist);
    if( ((Menor_distancia*)d)->dist <= ((Menor_distancia*)stripClsDist)->dist)
    {
        return d;
    }
    else
    {
        return stripClsDist;
    }
    /* Find the closest points in strip.  Return the minimum of d and closest
    distance is strip[]*/
}
 
// Driver program to test above functions
void closest_pair(Lista LISTA_TORRES)
{
    int n = length_lista(LISTA_TORRES);
    Menor_distancia *resultado;

    Point **vetor_torres = transforming_list(LISTA_TORRES);
    Point **torres_x = vetor_torres;
    Point **torres_y = vetor_torres;
    margeSortx(torres_x,0,n-1);
    margeSorty(torres_y,0,n-1);
    closestUtil(torres_x,torres_y,n);
    printf("\n\nOI\n\n");
    // printf("The smallest distance is %f ", closest(P, n));
}