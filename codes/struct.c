#include "../headers/includes.h"

Colors *newColors()
{
    Colors *cor = (Colors*)malloc(sizeof(Colors));

    cor->corBorder = NULL;
    cor->corFill = NULL;
    return cor;
}

Arguments *newArguments()
{
  Arguments *arg = (Arguments*)malloc(sizeof(Arguments));
  
  arg->path = NULL;
  arg->directory = NULL;
  arg->filePosGEO = NULL;
  arg->filePosQRY = NULL;
  arg->geoFile = NULL;
  arg->qryFile = NULL;
  arg->svgFile = NULL;
  arg->txtFile = NULL;
  arg->mainFile = NULL;
  arg->sFile = NULL;
  arg->tFile = NULL;
  arg->parada = 0;
  return arg;
}

void freeArguments(Arguments **arg)
{
  free((*arg)->path);
  free((*arg)->directory);
  free((*arg)->filePosGEO);
  free((*arg)->filePosQRY);
  free((*arg)->mainFile);
  free((*arg)->geoFile); 
  free((*arg)->qryFile); 
  free((*arg)->svgFile); 
  free((*arg)->txtFile); 
  free((*arg)->mainFile); 
  free((*arg)->sFile); 
  free((*arg)->tFile); 

  free(*arg);
  *arg = NULL;
}

Figure *newFigure()
{
    Figure *fig = (Figure*)malloc(sizeof(Figure)); 

    fig->id = NULL;

    fig->radius = 0;
    fig->width = 0;
    fig->height = 0;

    fig->x = 0;
    fig->y = 0;

    fig->perColor = NULL; /*cor da borda*/
    fig->fillColor = NULL; /*cor do preenchimento*/

    fig->xcenter = 0;
    fig->ycenter = 0;

    return fig;
}

void freeFigures(Figure **fig)
{
  free((*fig)->id);
  free((*fig)->perColor);
  free((*fig)->fillColor);

  free(*fig);
  *fig = NULL;
}