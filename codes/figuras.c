#include "../headers/includes.h"

//
    Quadra newQuadra(){
    struct Quadra *q = (struct Quadra*)malloc(sizeof(struct Quadra));
    
    q->tipo = (char*)malloc(sizeof(char));
    strcpy(q->tipo,"q");
    q->id = NULL;
    q->width = 0;
    q->height = 0;
    q->x = 0;
    q->y = 0;
    q->xcenter = 0;
    q->ycenter = 0;
    q->perColor = NULL;
    q->fillColor = NULL;

    return q;
    }

    Hidrante newHidrante(){
        struct Hidrante *h = (struct Hidrante*)malloc(sizeof(struct Hidrante));
        
        h->tipo = (char*)malloc(sizeof(char));
        strcpy(h->tipo,"h");
        h->id = 0;
        h->x = 0;
        h->y = 0;
        h->perColor = NULL;
        h->fillColor = NULL;

        return h;
    }

    Semaforo newSemaforo(){
        struct Semaforo *s = (struct Semaforo*)malloc(sizeof(struct Semaforo));
        
        s->tipo = (char*)malloc(sizeof(char));
        strcpy(s->tipo,"s");
        s->id = 0;
        s->x = 0;
        s->y = 0;
        s->perColor = NULL;
        s->fillColor = NULL;
        return s;   
    }

    Radio newRadio(){
        struct Radio *r = (struct Radio*)malloc(sizeof(struct Radio));
        
        r->tipo = (char*)malloc(sizeof(char));
        strcpy(r->tipo,"r");
        r->id = 0;
        r->x = 0;
        r->y = 0;
        r->perColor = NULL;
        r->fillColor = NULL;

        return r;   
    }
//
void localizarArea(Lista* VETOR_LISTAS,char* inst,int tipo,int tipo2,Arguments* arg)
{
    char* aux;
    char* aux2;
    float x,y,width,height,radius;
    char* equipamento = alocarMemoria(4);

    Figure* figura;
    Posic p;
    Lista OBJ_PRESENTES;
    strcpy(equipamento," ");
    OBJ_PRESENTES = create_lista();

    aux  = alocarMemoria( strlen(inst));
    aux2 = alocarMemoria( strlen(inst));
    strcpy(aux,inst);
    aux = cut_until_char(aux);
    if(tipo2 == SIM|| tipo2 == NADA)
    {
        if(tipo == RETANGULO)
        {
            figura = newFigure();
            aux2 = separate_char(aux,aux2);
            x = atof(aux2);

            aux2 = separate_char(aux,aux2);
            y = atof(aux2);

            aux2 = separate_char(aux,aux2);
            width = atof(aux2);

            aux2 = separate_char(aux,aux2);
            height = atof(aux2);

            equipamento[0] = 'q';

            figura->x = x;
            figura->y = y;
            figura->width = width;
            figura->height = height;
            figura->id = alocarMemoria(12);
            strcpy(figura->id,"PONTILHADO");
            insert_lista(VETOR_LISTAS[FIGS_EXTRA],(Item) figura);
        }else if(tipo == CIRCULO)
        {
            figura = newFigure();
            aux2 = separate_char(aux,aux2);
            radius = atof(aux2);

            aux2 = separate_char(aux,aux2);
            x = atof(aux2);

            aux2 = separate_char(aux,aux2);
            y = atof(aux2);

            equipamento[0] = 'q';

            figura->x = x;
            figura->y = y;
            figura->radius = radius;
            figura->id = alocarMemoria(12);
            strcpy(figura->id,"PONTILHADO");
            insert_lista(VETOR_LISTAS[FIGS_EXTRA],(Item) figura);
        }
    }else if(tipo2 == NAO)
    {
        if(tipo == RETANGULO)
        {
            aux2 = separate_char(aux,aux2);
            strcpy(equipamento,aux2);

            figura = newFigure();
            aux2 = separate_char(aux,aux2);
            x = atof(aux2);

            aux2 = separate_char(aux,aux2);
            y = atof(aux2);

            aux2 = separate_char(aux,aux2);
            width = atof(aux2);

            aux2 = separate_char(aux,aux2);
            height = atof(aux2);

            figura->x = x;
            figura->y = y;
            figura->width = width;
            figura->height = height;
            figura->id = alocarMemoria(12);
            strcpy(figura->id,"PONTILHADO");
            insert_lista(VETOR_LISTAS[FIGS_EXTRA],(Item) figura);
        }else if(tipo == CIRCULO)
        {
            figura = newFigure();
            aux2 = separate_char(aux,aux2);
            strcpy(equipamento,aux2);

            aux2 = separate_char(aux,aux2);
            radius = atof(aux2);

            aux2 = separate_char(aux,aux2);
            x = atof(aux2);

            aux2 = separate_char(aux,aux2);
            y = atof(aux2);


            figura->x = x;
            figura->y = y;
            figura->radius = radius;
            figura->id = alocarMemoria(12);
            strcpy(figura->id,"PONTILHADO");
            insert_lista(VETOR_LISTAS[FIGS_EXTRA],(Item) figura);
        }
    }

    if(tipo2 == NADA)
    {
        informar_presenca(VETOR_LISTAS,OBJ_PRESENTES," ");
    }
    else
    {
        if(tipo2 == SIM)
        {
            informar_presenca(VETOR_LISTAS,OBJ_PRESENTES,"q");
        }
        else
        {
            informar_presenca(VETOR_LISTAS,OBJ_PRESENTES,equipamento);
        }
    }

    if(tipo2 == NADA)
    {
        reportar_figuras(OBJ_PRESENTES,arg);
    }
    else
    {
        remover_figuras(VETOR_LISTAS,OBJ_PRESENTES,arg);
    }
    destruir_lista(OBJ_PRESENTES,NULL);
    free(aux);
    free(aux2);
    
}

void informar_presenca(Lista* VETOR_LISTAS,void* OBJ_PRESENTES,char* equipamento)
{
    Posic p_percorre;
    Posic p_add;
    Posic p_figura;
    void* figura;
    void* objeto;
    int i=0;

    p_figura = get_last_lista(VETOR_LISTAS[FIGS_EXTRA]);
    figura = get_lista(VETOR_LISTAS[FIGS_EXTRA],p_figura);

    p_add =get_first_lista((Lista)OBJ_PRESENTES);

    if( ((Figure*)figura)->radius == 0 )
    {
        if(equipamento == " ")
        {
            while(i<FIGS)
            {
                p_percorre = get_first_lista(VETOR_LISTAS[i]);
                objeto = get_lista(VETOR_LISTAS[i],p_percorre);
                while(p_percorre)
                {
                    if(i == QUADRA)
                    {
                        if( ((Figure*)figura)->x+((Figure*)figura)->width >= ((struct Quadra*)objeto)->x + ((struct Quadra*)objeto)->width)
                        {
                            if(((struct Quadra*)objeto)->x>=((Figure*)figura)->x)
                            {
                                if(((Figure*)figura)->y+((Figure*)figura)->height >= ((struct Quadra*)objeto)->y + ((struct Quadra*)objeto)->height)
                                {
                                    if(((struct Quadra*)objeto)->y>=((Figure*)figura)->y)
                                    {
                                        insert_lista((Lista)OBJ_PRESENTES,objeto);
                                    }
                                }
                            }
                        }
                    }
                    if(i == HIDRANTE)
                    {
                        if( ((Figure*)figura)->x+((Figure*)figura)->width >= ((struct Hidrante*)objeto)->x)
                        {
                            if(((struct Hidrante*)objeto)->x>=((Figure*)figura)->x)
                            {
                                if(((Figure*)figura)->y+((Figure*)figura)->height >= ((struct Hidrante*)objeto)->y)
                                {
                                    if(((struct Hidrante*)objeto)->y>=((Figure*)figura)->y)
                                    {
                                        insert_lista((Lista)OBJ_PRESENTES,objeto);
                                    }
                                }
                            }
                        }
                    }
                    if(i == SEMAFORO)
                    {
                        if( ((Figure*)figura)->x+((Figure*)figura)->width >= ((struct Semaforo*)objeto)->x)
                        {
                            if(((struct Semaforo*)objeto)->x>=((Figure*)figura)->x)
                            {
                                if(((Figure*)figura)->y+((Figure*)figura)->height >= ((struct Semaforo*)objeto)->y)
                                {
                                    if(((struct Semaforo*)objeto)->y>=((Figure*)figura)->y)
                                    {
                                        insert_lista((Lista)OBJ_PRESENTES,objeto);
                                    }
                                }
                            }
                        }
                    }
                    if(i == TORRE_RADIO)
                    {
                        if( ((Figure*)figura)->x+((Figure*)figura)->width >= ((struct Radio*)objeto)->x)
                        {
                            if(((struct Radio*)objeto)->x>=((Figure*)figura)->x)
                            {
                                if(((Figure*)figura)->y+((Figure*)figura)->height >= ((struct Radio*)objeto)->y)
                                {
                                    if(((struct Radio*)objeto)->y>=((Figure*)figura)->y)
                                    {
                                        insert_lista((Lista)OBJ_PRESENTES,objeto);
                                    }
                                }
                            }
                        }
                    }
                    p_percorre = get_next_lista(VETOR_LISTAS[i],p_percorre);
                    objeto = get_lista(VETOR_LISTAS[i],p_percorre);
                }
                i++;
            }
        }
        if(equipamento[0] == 'q'||equipamento[1] == 'q'||equipamento[2]=='q')
        {
            p_percorre = get_first_lista(VETOR_LISTAS[ QUADRA ]);
            objeto = get_lista(VETOR_LISTAS[ QUADRA ],p_percorre);
            while(p_percorre)
            {
                if( ((Figure*)figura)->x+((Figure*)figura)->width >= ((struct Quadra*)objeto)->x + ((struct Quadra*)objeto)->width)
                {
                    if(((struct Quadra*)objeto)->x>=((Figure*)figura)->x)
                    {
                        if(((Figure*)figura)->y+((Figure*)figura)->height >= ((struct Quadra*)objeto)->y + ((struct Quadra*)objeto)->height)
                        {
                            if(((struct Quadra*)objeto)->y>=((Figure*)figura)->y)
                            {
                                insert_lista((Lista)OBJ_PRESENTES,objeto);
                                remove_lista(VETOR_LISTAS[ QUADRA ],p_percorre);
                            }
                        }
                    }
                }
                p_percorre = get_next_lista(VETOR_LISTAS[ QUADRA ],p_percorre);
                objeto = get_lista(VETOR_LISTAS[ QUADRA ],p_percorre);
            }
        }
        if(equipamento[0] == 'h'||equipamento[1] == 'h'||equipamento[2] == 'h')
        {
            p_percorre = get_first_lista(VETOR_LISTAS[ HIDRANTE ]);
            objeto = get_lista(VETOR_LISTAS[ HIDRANTE ],p_percorre);
            while(p_percorre)
            {
                if( ((Figure*)figura)->x+((Figure*)figura)->width >= ((struct Hidrante*)objeto)->x)
                {
                    if(((struct Hidrante*)objeto)->x>=((Figure*)figura)->x)
                    {
                        if(((Figure*)figura)->y+((Figure*)figura)->height >= ((struct Hidrante*)objeto)->y)
                        {
                            if(((struct Hidrante*)objeto)->y>=((Figure*)figura)->y)
                            {
                                insert_lista((Lista)OBJ_PRESENTES,objeto);
                                remove_lista(VETOR_LISTAS[ HIDRANTE ],p_percorre);
                            }
                        }
                    }
                }   
                p_percorre = get_next_lista(VETOR_LISTAS[ HIDRANTE ],p_percorre);
                objeto = get_lista(VETOR_LISTAS[ HIDRANTE ],p_percorre);
            }
        }
        if(equipamento[0] == 's'||equipamento[1] == 's'||equipamento[2] == 's')
        {
            p_percorre = get_first_lista(VETOR_LISTAS[ SEMAFORO ]);
            objeto = get_lista(VETOR_LISTAS[ SEMAFORO ],p_percorre);
            while(p_percorre)
            {
                if( ((Figure*)figura)->x+((Figure*)figura)->width >= ((struct Semaforo*)objeto)->x)
                {
                    if(((struct Semaforo*)objeto)->x>=((Figure*)figura)->x)
                    {
                        if(((Figure*)figura)->y+((Figure*)figura)->height >= ((struct Semaforo*)objeto)->y)
                        {
                            if(((struct Semaforo*)objeto)->y>=((Figure*)figura)->y)
                            {
                                insert_lista((Lista)OBJ_PRESENTES,objeto);
                                remove_lista(VETOR_LISTAS[ SEMAFORO ],p_percorre);
                            }
                        }
                    }
                }
                p_percorre = get_next_lista(VETOR_LISTAS[ SEMAFORO ],p_percorre);
                objeto = get_lista(VETOR_LISTAS[ SEMAFORO ],p_percorre);
            }
        }
        if(equipamento[0] == 'r'||equipamento[1] == 'r'||equipamento[2] == 'r')
        {
            p_percorre = get_first_lista(VETOR_LISTAS[ TORRE_RADIO]);
            objeto = get_lista(VETOR_LISTAS[ TORRE_RADIO],p_percorre);
            while(p_percorre)
            {
                if( ((Figure*)figura)->x+((Figure*)figura)->width >= ((struct Radio*)objeto)->x)
                {
                    if(((struct Radio*)objeto)->x>=((Figure*)figura)->x)
                    {
                        if(((Figure*)figura)->y+((Figure*)figura)->height >= ((struct Radio*)objeto)->y)
                        {
                            if(((struct Radio*)objeto)->y>=((Figure*)figura)->y)
                            {
                                insert_lista((Lista)OBJ_PRESENTES,objeto);
                                remove_lista(VETOR_LISTAS[ TORRE_RADIO ],p_percorre);
                            }
                        }
                    }
                }
                p_percorre = get_next_lista(VETOR_LISTAS[ TORRE_RADIO],p_percorre);
                objeto = get_lista(VETOR_LISTAS[ TORRE_RADIO],p_percorre);
            }
        }
    }
    else
    {
        if(equipamento == " ")
        {
            while(i<FIGS)
            {
                p_percorre = get_first_lista(VETOR_LISTAS[i]);
                objeto = get_lista(VETOR_LISTAS[i],p_percorre);
                while(p_percorre)
                {
                    if(i == QUADRA)
                    {

                        if( dotDistance(((Figure*)figura)->x,((Figure*)figura)->y,((struct Quadra*)objeto)->x,((struct Quadra*)objeto)->y) < ((Figure*)figura)->radius )
                        {
                            if(dotDistance(((Figure*)figura)->x,((Figure*)figura)->y,((struct Quadra*)objeto)->x + ((struct Quadra*)objeto)->width,((struct Quadra*)objeto)->y) < ((Figure*)figura)->radius)
                            {
                                if(dotDistance(((Figure*)figura)->x,((Figure*)figura)->y,((struct Quadra*)objeto)->x + ((struct Quadra*)objeto)->width,((struct Quadra*)objeto)->y + ((struct Quadra*)objeto)->height) < ((Figure*)figura)->radius)
                                {
                                    if(dotDistance(((Figure*)figura)->x,((Figure*)figura)->y,((struct Quadra*)objeto)->x,((struct Quadra*)objeto)->y + ((struct Quadra*)objeto)->height) < ((Figure*)figura)->radius)
                                    {
                                        insert_lista((Lista)OBJ_PRESENTES,objeto);
                                    }
                                }
                            }
                        }
                    }
                    if(i == HIDRANTE)
                    {
                        if( dotDistance(((Figure*)figura)->x,((Figure*)figura)->y,((struct Hidrante*)objeto)->x,((struct Hidrante*)objeto)->y) < ((Figure*)figura)->radius )
                        {
                            insert_lista((Lista)OBJ_PRESENTES,objeto);
                        }
                    }
                    if(i == SEMAFORO)
                    {
                        if( dotDistance(((Figure*)figura)->x,((Figure*)figura)->y,((struct Semaforo*)objeto)->x,((struct Semaforo*)objeto)->y) < ((Figure*)figura)->radius )
                        {
                            insert_lista((Lista)OBJ_PRESENTES,objeto);
                        }
                    }
                    if(i == TORRE_RADIO)
                    {
                        if( dotDistance(((Figure*)figura)->x,((Figure*)figura)->y,((struct Radio*)objeto)->x,((struct Radio*)objeto)->y) < ((Figure*)figura)->radius )
                        {
                            insert_lista((Lista)OBJ_PRESENTES,objeto);
                        }
                    }
                    p_percorre = get_next_lista(VETOR_LISTAS[i],p_percorre);
                    objeto = get_lista(VETOR_LISTAS[i],p_percorre);
                }
                i++;
            }
        }
        if(equipamento[0] == 'q'||equipamento[1] == 'q'||equipamento[2]=='q')
        {
            p_percorre = get_first_lista(VETOR_LISTAS[ QUADRA ]);
            objeto = get_lista(VETOR_LISTAS[ QUADRA ],p_percorre);
            while(p_percorre)
            {
                if( dotDistance(((Figure*)figura)->x,((Figure*)figura)->y,((struct Quadra*)objeto)->x,((struct Quadra*)objeto)->y) < ((Figure*)figura)->radius )
                {
                    if(dotDistance(((Figure*)figura)->x,((Figure*)figura)->y,((struct Quadra*)objeto)->x + ((struct Quadra*)objeto)->width,((struct Quadra*)objeto)->y) < ((Figure*)figura)->radius)
                    {
                        if(dotDistance(((Figure*)figura)->x,((Figure*)figura)->y,((struct Quadra*)objeto)->x + ((struct Quadra*)objeto)->width,((struct Quadra*)objeto)->y + ((struct Quadra*)objeto)->height) < ((Figure*)figura)->radius)
                        {
                            if(dotDistance(((Figure*)figura)->x,((Figure*)figura)->y,((struct Quadra*)objeto)->x,((struct Quadra*)objeto)->y + ((struct Quadra*)objeto)->height) < ((Figure*)figura)->radius)
                            {
                                insert_lista((Lista)OBJ_PRESENTES,objeto);
                                remove_lista(VETOR_LISTAS[ QUADRA ],p_percorre);
                            }
                        }
                    }
                }

                p_percorre = get_next_lista(VETOR_LISTAS[ QUADRA ],p_percorre);
                objeto = get_lista(VETOR_LISTAS[ QUADRA ],p_percorre);
            }
        }
        if(equipamento[0] == 'h'||equipamento[1] == 'h'||equipamento[2] == 'h')
        {
            p_percorre = get_first_lista(VETOR_LISTAS[ HIDRANTE ]);
            objeto = get_lista(VETOR_LISTAS[ HIDRANTE ],p_percorre);
            while(p_percorre)
            {
                if( dotDistance(((Figure*)figura)->x,((Figure*)figura)->y,((struct Hidrante*)objeto)->x,((struct Hidrante*)objeto)->y) < ((Figure*)figura)->radius )
                {
                    insert_lista((Lista)OBJ_PRESENTES,objeto);
                    remove_lista(VETOR_LISTAS[ HIDRANTE ],p_percorre);
                }
                p_percorre = get_next_lista(VETOR_LISTAS[ HIDRANTE ],p_percorre);
                objeto = get_lista(VETOR_LISTAS[ HIDRANTE ],p_percorre);
            }
        }
        if(equipamento[0] == 's'||equipamento[1] == 's'||equipamento[2] == 's')
        {
            p_percorre = get_first_lista(VETOR_LISTAS[ SEMAFORO ]);
            objeto = get_lista(VETOR_LISTAS[ SEMAFORO ],p_percorre);
            while(p_percorre)
            {
                if( dotDistance(((Figure*)figura)->x,((Figure*)figura)->y,((struct Semaforo*)objeto)->x,((struct Semaforo*)objeto)->y) < ((Figure*)figura)->radius )
                {
                    insert_lista((Lista)OBJ_PRESENTES,objeto);
                    remove_lista(VETOR_LISTAS[ SEMAFORO ],p_percorre);
                }
                p_percorre = get_next_lista(VETOR_LISTAS[ SEMAFORO ],p_percorre);
                objeto = get_lista(VETOR_LISTAS[ SEMAFORO ],p_percorre);
            }
        }
        if(equipamento[0] == 'r'||equipamento[1] == 'r'||equipamento[2] == 'r')
        {
            p_percorre = get_first_lista(VETOR_LISTAS[ TORRE_RADIO]);
            objeto = get_lista(VETOR_LISTAS[ TORRE_RADIO],p_percorre);
            while(p_percorre)
            {
                if( dotDistance(((Figure*)figura)->x,((Figure*)figura)->y,((struct Radio*)objeto)->x,((struct Radio*)objeto)->y) < ((Figure*)figura)->radius )
                {
                    insert_lista((Lista)OBJ_PRESENTES,objeto);
                    remove_lista(VETOR_LISTAS[ TORRE_RADIO ],p_percorre);
                }
                p_percorre = get_next_lista(VETOR_LISTAS[ TORRE_RADIO],p_percorre);
                objeto = get_lista(VETOR_LISTAS[ TORRE_RADIO],p_percorre);
            }
        }
    }
}

void reportar_figuras(Lista OBJ_PRESENTES,Arguments *arg)
{
    Posic p = get_first_lista(OBJ_PRESENTES);
    void* figura = get_lista(OBJ_PRESENTES,p);

    fprintf(arg->tFile,"\n--FIGURAS_PRESENTES_NA_AREA_MARCADA--\n");
    while(p)
    {
        if( strcmp( ((struct Quadra*)figura)->tipo,"q") == 0)
        {
            fprintf(arg->tFile,"\nCEP = %s X = %.5f Y = %.5f LARGURA = %.5f ALTURA = %.5f",((struct Quadra*) figura)->id, ((struct Quadra*) figura)->x, ((struct Quadra*) figura)->y, ((struct Quadra*) figura)->width, ((struct Quadra*) figura)->height);
        }
        else
        {
            fprintf(arg->tFile,"\nID = %s X = %.5f Y = %.5f",((struct Quadra*) figura)->id, ((struct Quadra*) figura)->x, ((struct Quadra*) figura)->y);
        }
        p = get_next_lista(OBJ_PRESENTES,p);
        figura = get_lista(OBJ_PRESENTES,p);
    }
    free(p);
}

void remover_figuras(Lista* VETOR_LISTAS,Lista OBJ_PRESENTES,Arguments *arg)
{
    Posic p = get_first_lista(OBJ_PRESENTES);
    void* figura = get_lista(OBJ_PRESENTES,p);

    fprintf(arg->tFile,"\n--FIGURAS_REMOVIDAS_DA_AREA_MARCADA--\n\n");
    while(p)
    {
        if( strcmp( ((struct Quadra*)figura)->tipo,"q") == 0)
        {
            fprintf(arg->tFile,"CEP REMOVIDO = %s\n",((struct Quadra*) figura)->id);
        }
        else
        {
            fprintf(arg->tFile,"ID REMOVIDO = %s\n",((struct Quadra*) figura)->id);
        }
        p = get_next_lista(OBJ_PRESENTES,p);
        figura = get_lista(OBJ_PRESENTES,p);
    }
    free(p);
    free(figura);
}

void criar_figura(Lista*VETOR_LISTAS,char* inst,int tipo)
{
    Posic p;
    Figure* figura;
    char* aux;
    char* aux2;
    aux  = alocarMemoria( strlen(inst));
    aux2 = alocarMemoria( strlen(inst));
    
    strcpy(aux,inst);
    aux = cut_until_char(aux);

    if(tipo == CIRCULO)
    {
        figura = newFigure();
        insert_info_fig(figura,aux,aux2,CIRCULO);
        
        p = insert_lista(VETOR_LISTAS[FIGS],(Item)figura);
        
    }
    if(tipo == RETANGULO)
    {
        figura = newFigure();
        insert_info_fig(figura,aux,aux2,RETANGULO);

        p = insert_lista(VETOR_LISTAS[FIGS],(Item)figura);
    }
    free(aux);
    free(aux2);
}

void insert_info_fig(Figure* figura,char* aux,char* aux2,int tipo)
{
    int i;
    if(tipo == CIRCULO)
    {
        aux2 = separate_char(aux,aux2);
        i = strlen(aux2);
        figura->id = alocarMemoria(i);
        strcpy( figura->id,aux2);

        aux2 = separate_char(aux,aux2);
        i = strlen(aux2);
        figura->perColor = alocarMemoria(i);
        strcpy( figura->perColor,aux2);
        
        aux2 = separate_char(aux,aux2);
        i = strlen(aux2);
        figura->fillColor = alocarMemoria(i);
        strcpy( figura->fillColor,aux2);

        aux2 = separate_char(aux,aux2);
        figura->radius = atof(aux2);

        aux2 = separate_char(aux,aux2);
        figura->x = atof(aux2);
        figura->xcenter = atof(aux2);
        
        aux2 = separate_char(aux,aux2);
        figura->y = atof(aux2);
        figura->ycenter = atof(aux2);
    }
    else
    {
        aux2 = separate_char(aux,aux2);
        figura->id = alocarMemoria( strlen(aux2));
        strcpy( figura->id,aux2);

        aux2 = separate_char(aux,aux2);
        figura->perColor = alocarMemoria( strlen(aux2));
        strcpy( figura->perColor,aux2);
        
        aux2 = separate_char(aux,aux2);
        figura->fillColor = alocarMemoria( strlen(aux2));
        strcpy( figura->fillColor,aux2);

        aux2 = separate_char(aux,aux2);
        figura->width = atof(aux2);

        aux2 = separate_char(aux,aux2);
        figura->height = atof(aux2);

        aux2 = separate_char(aux,aux2);
        figura->x = atof(aux2);
        
        aux2 = separate_char(aux,aux2);
        figura->y = atof(aux2);

        figura->xcenter = (figura->x + figura->x + figura->width) / 2;
        figura->ycenter = (figura->y + figura->y + figura->height) / 2;

    }

}


