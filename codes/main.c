#include "../headers/includes.h"

int main(int argc, const char* argv[])
{
    Arguments *arg = argumentos(argc,argv); // struct que possui os parametros passados pela linha de comando
    FILE *mainFile;
    Lista VETOR_LISTAS[6];

    VETOR_LISTAS[ QUADRA ] = create_lista();
    VETOR_LISTAS[ HIDRANTE ] = create_lista();
    VETOR_LISTAS[ SEMAFORO ] = create_lista();
    VETOR_LISTAS[ TORRE_RADIO ] = create_lista();
    VETOR_LISTAS[ FIGS ] = create_lista();
    VETOR_LISTAS[ FIGS_EXTRA ] = create_lista();
    //manipulando lista
    //NÃO TENHO REFERENCIAS A NEWQUADRA, APENAS INICIOooooooooooooooo
        // insert_lista(VETOR_LISTAS[ QUADRA ], (Item) newQuadra());
        
        // Posic p = get_first_lista(VETOR_LISTAS[QUADRA]);
        
        // Quadra *quad = (Quadra*)get_lista(VETOR_LISTAS[QUADRA], p);
        // quad->x = 30;
        
        // insert_lista(VETOR_LISTAS[ QUADRA ], (Item) newQuadra());
        // p = get_next_lista(VETOR_LISTAS[QUADRA],p);

        // quad = get_lista(VETOR_LISTAS[QUADRA], p);
        // quad->x = 50;
    //aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
    abrindoArquivo(&arg->mainFile,arg->filePosGEO,0);
    abrindoArquivo(&arg->sFile,arg->svgFile,1);
    abrindoArquivo(&arg->tFile,arg->txtFile,1);
    lendoArquivo(arg,VETOR_LISTAS);

    fclose(arg->mainFile);
    draw_svg(VETOR_LISTAS,arg);
    fclose(arg->sFile);
    fclose(arg->tFile);

    if(arg->filePosQRY != NULL)
    {
        alter_nome_arquivo(SVG,arg);
        abrindoArquivo(&arg->mainFile,arg->filePosQRY,0);
        abrindoArquivo(&arg->tFile,arg->txtFile,2);
        abrindoArquivo(&arg->sFile,arg->svgFile,1);

        lendoArquivo(arg,VETOR_LISTAS);
        fclose(arg->mainFile);
        draw_svg(VETOR_LISTAS,arg);
        fclose(arg->sFile);
        fclose(arg->tFile);

    }
    
    destruir_lista(VETOR_LISTAS[QUADRA], NULL);
    destruir_lista(VETOR_LISTAS[HIDRANTE], NULL);
    destruir_lista(VETOR_LISTAS[SEMAFORO], NULL);
    destruir_lista(VETOR_LISTAS[TORRE_RADIO], NULL);
    destruir_lista(VETOR_LISTAS[FIGS], NULL);
    destruir_lista(VETOR_LISTAS[FIGS_EXTRA], NULL);

    printf("CORRETO!\n");
    return 0;
}
